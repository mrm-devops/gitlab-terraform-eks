variable "region" {
  default     = "us-east-2"
  description = "AWS region"
}

variable "cidr_block" {
  default     = "10.0.0.0/16"
  description = "CIDR block for the VPC"
}

variable "private_subnets_cidr_blocks" {
  default     = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
  description = "CIDR blocks for the private subnets"
}

variable "public_subnets_cidr_blocks" {
  default     = ["10.0.4.0/24", "10.0.5.0/24", "10.0.6.0/24"]
  description = "CIDR blocks for the public subnets"
}

variable "cluster_name" {
  default     = "gitlab-terraform-eks"
  description = "EKS Cluster name"
}

variable "cluster_version" {
  default     = "1.22"
  description = "Kubernetes version"
}

variable "instance_type" {
  default     = "t3.small"
  description = "EKS node instance type"
}

variable "instance_count_desired" {
  default     = 1
  description = "EKS node count desired"
}

variable "instance_count_max" {
  default     = 3
  description = "EKS node count max"
}

variable "agent_version" {
  default     = "v15.0.0"
  description = "Agent version"
}

variable "agent_namespace" {
  default     = "gitlab-agent"
  description = "Kubernetes namespace to install the Agent"
}

variable "agents_token" {
  description = "Agents token (provided after registering an Agent in GitLab)"
  type        = list(string)
  sensitive   = true
}

variable "kas_address" {
  description = "Agent Server address (provided after registering an Agent in GitLab)"
}

variable "domain_address" {
  description = "Domain address (domain with wildcard registred in route53)"
}

variable "certificate_arn" {
  description = "Certificate ARN (certificate registred in ACM with DNS validation)"
}