resource "kubernetes_namespace" "monitoring_namespace" {
  metadata {
    name = "gitlab-managed-monitoring"
  }
}

resource "helm_release" "prometheus_stack" {
  name = "prometheus-stack"

  repository = "https://prometheus-community.github.io/helm-charts"
  chart      = "kube-prometheus-stack"
  namespace  = "gitlab-managed-monitoring"
}