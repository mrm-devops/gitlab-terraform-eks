output "cluster_endpoint" {
  description = "Endpoint for EKS control plane."
  value       = module.eks.cluster_endpoint
}

output "cluster_certificate" {
  description = "Certificate for EKS control plane."
  value       = data.aws_eks_cluster.cluster.certificate_authority.0.data
}