resource "kubernetes_namespace" "traefik_namespace" {
  metadata {
    name = "gitlab-managed-traefik"
  }
}

resource "helm_release" "traefik" {
  name = "traefik"

  repository = "https://helm.traefik.io/traefik"
  chart      = "traefik"
  namespace  = "gitlab-managed-traefik"

  set {
    name  = "ports.web.redirectTo"
    value = "websecure"
  }
}

resource "kubernetes_annotations" "traefik_certificate" {
  depends_on  = [helm_release.traefik]
  api_version = "v1"
  kind        = "Service"

  metadata {
    name      = "traefik"
    namespace = "gitlab-managed-traefik"
  }

  annotations = {
    "service.beta.kubernetes.io/aws-load-balancer-alpn-policy"     = "HTTP2Preferred"
    "service.beta.kubernetes.io/aws-load-balancer-ssl-cert"        = "${var.certificate_arn}"
    "service.beta.kubernetes.io/aws-load-balancer-ssl-ports"       = "websecure"
    "external-dns.alpha.kubernetes.io/hostname"                    = "*.${var.domain_address}"
    "service.beta.kubernetes.io/aws-load-balancer-ip-address-type" = "ipv4"
  }
}
