resource "kubernetes_namespace" "argocd_namespace" {
  metadata {
    name = "gitlab-managed-argocd"
  }
}

resource "helm_release" "argocd" {
  name = "argocd"

  chart     = "argocd"
  namespace = "gitlab-managed-argocd"
  version   = "0.2.0"
}
