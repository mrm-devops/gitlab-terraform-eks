
module "eks-external-dns" {
  source  = "lablabs/eks-external-dns/aws"
  version = "1.0.0"

  cluster_identity_oidc_issuer     = module.eks.oidc_provider
  cluster_identity_oidc_issuer_arn = module.eks.oidc_provider_arn
}
